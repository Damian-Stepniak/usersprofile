import {IError} from '../Error/error.model';

export interface IResponse<T> {
    errors: Array<IError>,
    response: T
    status: boolean
} 