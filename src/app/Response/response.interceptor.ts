import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Injectable } from "@angular/core";
import { finalize } from 'rxjs/operators';
import {environment} from "../../environments/environment";

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private spinner: NgxSpinnerService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.includes(environment.url)) {
      this.spinner.show();
    }

    return next.handle(request)
    .pipe(
      finalize(() => {
        if (request.url.includes(environment.url)) {
          var context = this;

          setTimeout(function(){ 
            context.spinner.hide();
           }, 1000);
        }
      })
    );
  }
}