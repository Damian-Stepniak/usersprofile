import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private toastr: ToastrService, 
    private translate: TranslateService) 
  {}

 intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   return next.handle(request)
     .pipe(
       retry(1),
       catchError((error: HttpErrorResponse) => {
        let errorMessage = '';
        let errorCode = '';

         if (error.error instanceof ErrorEvent) {
            console.log("A");
           errorMessage = `Error: ${error.error.message}`;
         } else {
           errorMessage = 'Down';
           errorCode = `${error.status}`;
         }
         
        this.translate.get([errorCode, errorMessage, 'Error']).subscribe((res: string) => {
            this.toastr.error(`${res[errorCode]} ${res[errorMessage]}`, res['Error'], {
              timeOut: 3000
            });
        });

         return throwError(errorMessage);
       })
     )
 }
}