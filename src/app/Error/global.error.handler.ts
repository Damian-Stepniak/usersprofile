import { ErrorHandler, Injectable} from '@angular/core';
import { IError } from './error.model';
import {TranslateService} from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private toastr: ToastrService, 
    private translate: TranslateService) 
  {}

  handleError(error: Array<IError>) {
    if (error.length != 0) {
        var code = error[0].code;
        var message = error[0].message;
        this.translate.get([`${code}`, `${message}`, 'Error'])
            .subscribe((res: string) => {
                this.toastr.error(`${res[`${code}`]} ${res[`${message}`]}`, res['Error'], {
                    timeOut: 2000,
                    progressBar: true
                });
        });
    } else {
        this.translate.get(['-1', 'Error']).subscribe((res: string) => {
            this.toastr.error(`${res['-1']}`, res['Error'], {
                timeOut: 2000,
                progressBar: true
            });
        });
    }
  }
}