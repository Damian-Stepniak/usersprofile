import { Injectable } from "@angular/core";
import {HttpClient} from '@angular/common/http';
import {IGetAllRequest, 
    ICreateProfileAttributeRequest, 
    IUpdateProfileAttributeRequest,
    IUpdateNumberProfileAttributeRequest
} from './profile.request.model';
import {IGetAllResponse, IGetProfileAttributeResponse, IBoolResponse} from './profile.response.model';
import {environment} from "../../environments/environment";
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private readonly url: string;

    public profileChanged: BehaviorSubject<IGetAllResponse> = new BehaviorSubject<IGetAllResponse>(null);

    constructor(private http: HttpClient) {
        this.url = environment.url;
    }

    getAll(request: IGetAllRequest) : Observable<IGetAllResponse> {
        return this.http.get<IGetAllResponse>(`${this.url}/api/ProfileAttribute/${request.userName}`);
    }

    addProfilAttribute(request: ICreateProfileAttributeRequest) : Observable<IGetProfileAttributeResponse> {
        return this.http.post<IGetProfileAttributeResponse>(`${this.url}/api/ProfileAttribute`, request);
    }

    updateProfilAttribute(request: IUpdateProfileAttributeRequest) : Observable<IGetProfileAttributeResponse> {
        return this.http.put<IGetProfileAttributeResponse>(`${this.url}/api/ProfileAttribute`, request);
    }

    deleteProfilAttribute(id: number) : Observable<IBoolResponse> {
        return this.http.delete<IBoolResponse>(`${this.url}/api/ProfileAttribute/${id}`);
    }

    moveNumber(req: IUpdateNumberProfileAttributeRequest) : Observable<IBoolResponse> {
        return this.http.post<IBoolResponse>(`${this.url}/api/ProfileAttribute/UpdateNumber`, req);
    }

    uploadFile(req: FormData) : Observable<IBoolResponse> {
        return this.http.put<IBoolResponse>(`${this.url}/api/ProfileAttribute/UploadFile`, req);
    }
}