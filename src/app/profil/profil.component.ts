import { Component, OnInit, ViewChild  } from '@angular/core';
import {ProfileService} from './profile.service';
import { IGetAll, IProfileAttribue } from './profile.model';
import { IGetAllResponse, IGetProfileAttributeResponse, IBoolResponse } from './profile.response.model';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {MatDialog} from '@angular/material';
import { EditComponent } from './dialogEdit/edit.component';
import { IUpdateProfileAttributeRequest } from './profile.request.model';
import { ActivatedRoute, Router } from "@angular/router";
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {environment} from "../../environments/environment";
import { DomSanitizer } from '@angular/platform-browser';

const AttributeType = {
  publication: 0,
  conference: 2,
  projectsResearch: 4
};

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  private readonly url: string;

  allProfileData: IGetAll;
  publicationEditModel = '';
  conferenceEditModel = '';
  projectsResearchEditModel = '';
  base64Image: any;
  editable = false;
  userNotFound = 0;

  @ViewChild('file') file;

  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '5rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no'
  }
  
  constructor(
    private profileServcie: ProfileService, 
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private _sanitizer: DomSanitizer,
    private router: Router) { 
      this.url = environment.url;
      this.route.queryParams.subscribe(val => this.getAllProfileAttribute());
    }

  openEditAboutMeDialog(): void {
    if (this.allProfileData.aboutMe == null) {
      this.allProfileData.aboutMe = {
        description: "",
        id: 0
      };
    }

    const dialogRef = this.dialog.open(EditComponent, {
      data: {description: this.allProfileData.aboutMe.description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (typeof result === 'undefined') {
        return;
      }
      
      this.allProfileData.aboutMe.description = result;

      this.updateProfileAttribute({
        description: this.allProfileData.aboutMe.description,
        id: this.allProfileData.aboutMe.id,
        number: 1
      }, 0);
    });
  }

  openEditContactDialog(): void {
    if (this.allProfileData.contact == null) {
      this.allProfileData.contact = {
        description: "",
        id: 0
      };
    }

    const dialogRef = this.dialog.open(EditComponent, {
      data: {description: this.allProfileData.contact.description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (typeof result === 'undefined') {
        return;
      }

      this.allProfileData.contact.description = result;

      this.updateProfileAttribute({
        description: this.allProfileData.contact.description,
        id: this.allProfileData.contact.id,
        number: 1
      }, 1);
    });
  }
  
  ngOnInit() {
  }

  getAllProfileAttribute() {
    this.profileServcie.getAll({userName: this.route.snapshot.paramMap.get("userName")})
      .subscribe((res: IGetAllResponse) => {
        if (res.status) {
          this.allProfileData = res.response.response;  
          this.editable = this.allProfileData.basic.isOwner;  
          
          if (this.allProfileData.image) {
            this.base64Image = this._sanitizer.bypassSecurityTrustUrl("data:Image/*;base64,"+this.allProfileData.image);
          } else {
            this.base64Image = "\/assets\/userProfil.jpg"
          }

          this.userNotFound = 1;

          return;
        }

        this.userNotFound = -1;

        //TODO Albo widok ze brak usera albo przelogowanie do Home
        this.router.navigateByUrl(``);

        throw res.errors;
      });
  } 
  
  updateProfileAttribute(req: IUpdateProfileAttributeRequest, type: number) {
    this.profileServcie.updateProfilAttribute(req)
      .subscribe((res: IGetProfileAttributeResponse) => {
        if (!res.status) {
          throw res.errors;
        }
      });
  }

  addProfileAttribute(type: number) {
    var description = "";

    switch (type) {
      case AttributeType.publication:
        description = this.publicationEditModel;
        this.publicationEditModel = "";
        break;
    case AttributeType.conference:
        description = this.conferenceEditModel;
        this.conferenceEditModel = "";
        break;
    case AttributeType.projectsResearch:
        description = this.projectsResearchEditModel;
        this.projectsResearchEditModel = "";
        break;
      default:
        break;
    }

    var req = {
      number: 1, 
      description: description, 
      profileAttributeIdent: type
    }

    this.profileServcie.addProfilAttribute(req)
      .subscribe((res: IGetProfileAttributeResponse) => {
        if (res.status) {
          this.getAllProfileAttribute();
        } else {
          throw res.errors;
        }
      });
  }

  deleteProfileAttribute(id: number) {
    this.profileServcie.deleteProfilAttribute(id)
      .subscribe((res: IBoolResponse) => {
        if (res.status) {
          this.getAllProfileAttribute();
        } else {
          throw res.errors;
        }
      })
  }

  uploadFile() {
    const formData: FormData = new FormData();
    formData.append('file', this.file.nativeElement.files[0], this.file.nativeElement.files[0].name);

    this.profileServcie.uploadFile(formData)
      .subscribe((res: IBoolResponse) => {
        if (res.status) {
          this.getAllProfileAttribute();
        } else {
          throw res.errors;
        }
      })
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  drop(event: CdkDragDrop<string[]>, type: number) {
    var elPrev: IProfileAttribue;
    var elCur: IProfileAttribue;

    switch (type) {
      case AttributeType.publication:
        elPrev = this.allProfileData.publications[event.previousIndex];
        elCur = this.allProfileData.publications[event.currentIndex];
        break;
    case AttributeType.conference:
        elPrev = this.allProfileData.conferences[event.previousIndex];
        elCur = this.allProfileData.conferences[event.currentIndex];
        break;
    case AttributeType.projectsResearch:
        elPrev = this.allProfileData.researchProjects[event.previousIndex];
        elCur = this.allProfileData.researchProjects[event.currentIndex];
        break;
      default:
        break;
    }

    this.profileServcie.moveNumber({
      prevId: elPrev.id,
      prevNumber: elPrev.number,
      curId: elCur.id,
      curNumber: elCur.number
    })
      .subscribe((res: IBoolResponse) => {
        if (res.status) {
          this.getAllProfileAttribute();
        } else {
          throw res.errors;
        }
    });
  }
}
