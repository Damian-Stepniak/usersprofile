import {IGetAll, IProfileAttribue} from './profile.model'
import {IResponse} from '../Response/response.model';

export interface IGetAllResponse extends IResponse<{response: IGetAll}> {}

export interface IGetProfileAttributeResponse extends IResponse<IProfileAttribue> {}

export interface IBoolResponse extends IResponse<boolean> {}