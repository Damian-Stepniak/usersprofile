interface IContact {
    id: number,
    description: string
}

interface IAboutMe {
    id: number,
    description: string
}

interface IPublications extends IProfileAttribue {
}

interface IConferences extends IProfileAttribue {
}

interface IResearchProjects extends IProfileAttribue {
}

interface ICourses extends IProfileAttribue {
}

interface IBasicInformation {
    email: string,
    userName: string,
    isOwner: boolean
}

export interface IProfileAttribue {
    id: number,
    description: string,
    number: number
}

export interface IGetAll {
    contact: IContact,
    aboutMe: IAboutMe,
    publications: Array<IPublications>,
    conferences: Array<IConferences>,
    researchProjects: Array<IResearchProjects>,
    courses: Array<ICourses>,
    basic: IBasicInformation,
    image: string
}