export interface IGetAllRequest {
    userName: string
}

export interface ICreateProfileAttributeRequest {
    number: number,
    description: string,
    profileAttributeIdent: number
}

export interface IUpdateProfileAttributeRequest {
    id: number,
    number: number,
    description: string,
}

export interface IUpdateNumberProfileAttributeRequest {
    prevId: Number,
    prevNumber: Number,
    curId: Number,
    curNumber: Number
}