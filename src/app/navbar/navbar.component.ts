import { Component, OnInit } from '@angular/core';
import {AuthService} from '../Auth/auth.service';
import {MatDialog} from '@angular/material';
import {LoginComponent} from '../Login/login.component';
import { ThemeService } from '../services/theme.service'; 
 import { Observable } from 'rxjs/Observable'; 

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isThemeDark: Observable<boolean>; 

  constructor(public auth: AuthService,
    public dialog: MatDialog,
    private themeService: ThemeService) { }

  ngOnInit() {
    this.isThemeDark = this.themeService.isThemeDark; 
  }
  
  toggleDarkTheme(checked: boolean) { 
    this.themeService.setDarkTheme(checked); 
    } 

  onOpenDialog() {
    const dialogRef = this.dialog.open(LoginComponent);
     dialogRef.afterClosed().subscribe(result => {
    });
  }

}
