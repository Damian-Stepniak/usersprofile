import { Component, OnInit, ViewChild } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { Observable } from 'rxjs/Observable';
import { MatPaginator, MatTableDataSource } from '@angular/material';


// na sztywno profile
export interface ProfileData {
  photo: string;
  name: string;
  work: string[];
  link: string;
}


const elements_data: ProfileData[] = [
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/8V46UZCS0V.jpg',
    name: 'Kamil Skarzynski',
    work: ['Technologie Mobilne','Programowanie niskopoziomowe'],
    link: 'Kamil-Skarzynski'
  },
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/LTLE4QGRVQ.jpg',
    name: 'Waldemar Bartyna',
    work: ['Programowanie Zaawansowane'],
    link: 'Waldemar-Bartyna'
  },
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/R926LU1YEA.jpg',
    name: 'Damian Stępniak',
    work: ['Programowanie Master Chef Class'],
    link: 'Kamil-Skarzynski'
  },
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/8V46UZCS0V.jpg',
    name: 'Jarosław Skaruz',
    work: ['Programowanie Obiektowe', 'Inżynieria Oprogramowania'],
    link: 'Kamil-Skarzynski'
  },
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/X1UK6NLGRU.jpg',
    name: 'Grzegorz Terlikowski',
    work: ['Platformy Programowania'],
    link: 'Kamil-Skarzynski'
  },
  {
    photo: 'https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/NO9CN3QYR3.jpg',
    name: 'Stanisław Ambroszkiewicz',
    work: ['Technologie Sieciowe'],
    link: 'Kamil-Skarzynski'
  },

];

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayedColumns = ['photo', 'name', 'work', 'link'];
  dataSource = new MatTableDataSource<ProfileData>(elements_data);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  isThemeDark: Observable<boolean>;
  constructor(private themeService: ThemeService) { }
  ngOnInit() {
    this.isThemeDark = this.themeService.isThemeDark;
    this.dataSource.paginator = this.paginator;
  }
  toggleDarkTheme(checked: boolean) {
    this.themeService.setDarkTheme(checked);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
