import {IResponse} from '../Response/response.model';
import {ILogin} from './auth.model';

export interface ILoginResponse extends IResponse<ILogin> {}