import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ILogin} from './auth.model';
import {ILoginRequest} from './auth.request.model';
import {ILoginResponse} from './auth.response.model';
import {environment} from "../../environments/environment";
import {Observable, ReplaySubject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private readonly url: string;

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();

    constructor(private http: HttpClient,
        private router: Router) {
        this.url = environment.url;
    }

    /**
     * Login
     * 
     * @param login 
     */
    login(login: ILoginRequest) : Observable<ILoginResponse> {
        return this.http.post<ILoginResponse>(`${this.url}/api/Account/Login`, login);
    }

    hasAuthenticated() {
       var token = this.getTokenFromStorage();
       if (token) {
           this.isAuthenticatedSubject.next(true);
           return true;
       }

       this.isAuthenticatedSubject.next(false);
       return false;
    }

    setAuth(auth: boolean) {
        this.isAuthenticatedSubject.next(auth);
    }

    setAccessResponseToStorage(data: ILogin) {
        sessionStorage.setItem("accessResponse", JSON.stringify(data));
        this.setAuth(true);
    }

    removeAccessResponseToStorage() {
        sessionStorage.removeItem("accessResponse");
    }

    getTokenFromStorage(): string {
        const accessResponse: ILogin = JSON.parse(sessionStorage.getItem('accessResponse'));
        if (accessResponse && accessResponse.token) {
          return accessResponse.token;
        }
        return null;
    }

    logout() {
        this.removeAccessResponseToStorage();
        
        this.router.navigateByUrl('').then();
    }
}