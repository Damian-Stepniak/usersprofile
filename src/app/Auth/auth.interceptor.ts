import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ILogin} from "./auth.model";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const accessResponse: ILogin = JSON.parse(sessionStorage.getItem('accessResponse'));

    if (accessResponse && accessResponse.token) {
      request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + accessResponse.token)});
    }

    //request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
    //request = request.clone({headers: request.headers.set('Accept', 'application/json')});

    return next.handle(request);
  }
}