export interface ILogin {
    id: string,
    username: string,
    token: string
}