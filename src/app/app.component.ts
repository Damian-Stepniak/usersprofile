import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {LoginComponent} from './Login/login.component';
import {AuthService} from './Auth/auth.service';
import {TranslateService} from "@ngx-translate/core";
import { Observable } from "rxjs/Observable"; 
import { ThemeService } from "./services/theme.service"; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ThemeService]
})
export class AppComponent implements OnInit {
  title = 'frontend';

  countrySelected = 1;
  isThemeDark: Observable<boolean>; 


  constructor(public dialog: MatDialog,
    public auth: AuthService,
    private translate: TranslateService,
    private themeService: ThemeService) {
      translate.addLangs(['en', 'pl']);
      translate.setDefaultLang('pl');
    }

  ngOnInit() {
    this.isThemeDark = this.themeService.isThemeDark;

    if (sessionStorage.getItem("translate")) {
      this.setCountry(Number.parseInt(sessionStorage.getItem("translate")));
    }
  }

  setCountry(type: number) {
    this.countrySelected = type;
    switch (this.countrySelected) {
      case 0:
        this.translate.setDefaultLang('en');
        break;
      case 1:
        this.translate.setDefaultLang('pl');
        break; 
      default:
        break;
    }

    sessionStorage.setItem("translate", this.countrySelected.toString());
  } 

  onOpenDialog() {
    const dialogRef = this.dialog.open(LoginComponent);
     dialogRef.afterClosed().subscribe(result => {
       
    });
  }
}