import { Component } from '@angular/core';
import {FormGroup, FormControl , Validators} from "@angular/forms";
import {AuthService} from '../auth/auth.service';
import {ILoginResponse} from '../auth/auth.response.model';
import {Router} from '@angular/router';
import {MatDialogRef} from '@angular/material';

@Component({
    selector: 'login-dialog',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    loginForm = new FormGroup({
        login: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
    });
    
    constructor(private loginService: AuthService,
        private router: Router,
        private dialogRef: MatDialogRef<LoginComponent>) {}

    ngOnInit(): void {
        this.dialogRef.updateSize('20%','auto');
     }
     
    onSubmit() {
        this.loginService.login(this.loginForm.value)
            .subscribe((res: ILoginResponse) => {
                if (res.status) {
                    this.loginService.setAccessResponseToStorage(res.response);
                    this.router.navigateByUrl(`/${res.response.username}?refresh=1`)
                        .then(() => {
                            this.dialogRef.close();
                        });
                } else {
                    console.log("Thrrow");
                    throw res.errors;
                }
            });
    }
}